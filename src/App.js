import './dist/css/main.min.css';
import React, { useEffect } from 'react';
import Header from './components/Header'; 
import Hp from './components/Hp'; 
import Footer from './components/Footer'; 
export default function App() {
  useEffect(() => {
    document.title = "Home | Proximity Indian Ocean";
  }, []);

  return (
    <div className="App" id="Main">
      <Header />
      <Hp />
      <Footer />
    </div>
  );
}