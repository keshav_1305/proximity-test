import React from 'react';
import VideoBanner from './Hp/VideoBanner'; 
import HowWeDoIt from './Hp/HowWeDoIt';
import BusinessProblems from './Hp/BusinessProblems';


export default function Hp() {
	return(
		<main className="main" id="main">
      <VideoBanner />
      <HowWeDoIt />
      <BusinessProblems />
    </main>
	);
}