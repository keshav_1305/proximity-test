import React, { useEffect, useState } from 'react';
import logo from '../assets/images/prox_primary_rw.png';

export default function Header() {
  // set sticky header on
  const [navOpen, setActive] = useState(false);
  const toggleActiveState = () => {
    setActive(!navOpen);
  };

  // Stick header on 
  const [hasScrolled, setScrolled] = useState(false);
  const handleScroll = () => {
    const y = window.scrollY;
    if (y > 400) {
      setScrolled(true);
    } else {
      setScrolled(false)
    }
  }

  useEffect(() => {
    window.addEventListener('scroll', handleScroll)
  });
  return (
    <header className={`main-header container${hasScrolled ? " sticky" : ""}`}>
      <a href="/">
        <img src={logo} className="logo" alt="logo" />
      </a>
      <nav className="desktop-nav">
        <ul className="nav-list">
          <li>
            <a href="/">About</a>
          </li>
          <li>
            <a href="/">Work</a>
          </li>
          <li>
            <a href="/">Latest</a>
          </li>
          <li>
            <a href="/">People & Careers</a>
          </li>
          <li>
            <a href="/">Contact</a>
          </li>
        </ul>
        <div className="language-selection">
          <ul className="lang-list">
            <li>
              <a href="/">EN</a>
            </li>
            <li>
              <a href="/">FR</a>
            </li>
          </ul>
        </div>
      </nav>
      <nav className={`mobile-nav${navOpen ? " open-menu" : ""}`}>
        <button className="toggle" onClick={toggleActiveState}>
          <span>Hamburger</span>
          <div className="hamburger">
            <div className="line one"></div>
            <div className="line two"></div>
            <div className="line three"></div>
          </div>
        </button>
        <div className="mobile-nav-list-wrap">
          <div className="language-selection">
            <ul className="lang-list">
              <li>
                <a href="/">EN</a>
              </li>
              <li>
                <a href="/">FR</a>
              </li>
            </ul>
          </div>
          <ul>
              <li>
                <a href="/">About</a>
              </li>
              <li>
                <a href="/">Work</a>
              </li>
              <li>
                <a href="/">Latest</a>
              </li>
              <li>
                <a href="/">People &amp; Careers</a>
              </li>
              <li>
                <a href="/">Contact</a>
              </li>
          </ul>
        </div>
      </nav>
    </header>
  );
}
