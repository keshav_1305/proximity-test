import React from 'react';
import banner from '../../assets/images/hwd_banner.jpg';

export default function VideoBanner() {
	return(
		<div className="wrapper home-section1" id="how-we-do-it-bloc">
			<h2>How we do it</h2>
			<h3>We use data-driven creativity to solve business problems</h3>
			<section className="card">
				<img src={banner} alt="Banner bloc BG" />
				<div className="foot-text-wrap">
					<div className="foot-text">
						<p>By harnessing powerful insights and smart targeting. we're able to create behaviour-changing ideas and experiences that deliver value to brands. 
							Supported by our proprietary <a href="/">Creative Intelligence process</a>, <a href="/">unique tools</a> and <a href="/">global partners</a>,
							 we are able to put data at the heart of everything we do to orchestrate experiences that deliver creativity with precision and purpose.
						</p>
					</div>
				</div>
			</section>
		</div>
	);
}
