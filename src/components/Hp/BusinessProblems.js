import React from 'react';
import pencil from '../../assets/images/pencil-2-v2-cropped.gif';
import right from '../../assets/images/economist_sharper.jpg';
import left from '../../assets/images/cochlear_o.jpg';
export default function BusinessProblems() {
    return(
        <div className="business-problems">
            <img src={pencil} alt="Pencil animation" />
            <h2>Business Problems We've Solved</h2>
            <div className="container-div-links">
                <div className="div-links">
                    <a href="/">
                        <img src={left} alt="Left image" />
                        <div className="div-link-texts">
                            <h5>Can a love story double as hearing test?</h5>
                            <span className="span-link">Find out here</span>
                        </div>
                    </a>
                </div>
                <div className="div-links">
                    <a href="/">
                        <img src={right} alt="Right image" />
                        <div className="div-link-texts">
                            <h5>How close to an AFL player can you get?</h5>
                            <span className="span-link">Find out here</span>
                        </div>
                    </a>
                </div>
            </div>
            <a className="link-view-our-word" href="/">View our work</a>
        </div>
    );
}
