import React from 'react';
import video from '../../assets/videos/video.mp4';
import arrow from '../../assets/images/_Arrowwhite.png';

export default function VideoBanner() {
    return(
        <div className="video-banner">
            <video autoPlay muted loop>
                <source src={video} type="video/mp4" />
            </video>
            <div className="video-heading">
                <h1>We make people more valuable to brands</h1>
            </div>
            <div className="anchor-arrow-wrap">
                <a href="#how-we-do-it-bloc">
                    <img src={arrow} alt="anchor-arrow" />
                </a>
            </div>
        </div>
    );
}
