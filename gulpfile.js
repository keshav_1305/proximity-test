var gulp = require('gulp');
var sass = require('gulp-sass');
var rename = require('gulp-rename');

// var cssPath = 'app/scss/*.scss';
gulp.task('sass', function(){
    return gulp.src('src/scss/main.scss')
        .pipe(sass({
            outputStyle: 'compressed'
        }))
        .pipe( rename( { suffix: '.min' } ) )
        .pipe(gulp.dest('src/dist/css'));
});
//watch sass changes and compile
gulp.task('watch', function(){
    gulp.watch('src/**/*.scss',gulp.series('sass'));
});

gulp.task('sass');